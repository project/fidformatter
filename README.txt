
-- SUMMARY --

File ID formatter provides a field formatter for file and image fields that
prints just the file id.


-- REQUIREMENTS --

* The drupal core file module.
* (optional) The drupal core image module.


-- INSTALLATION --

Install as usual.
See https://drupal.org/documentation/install/modules-themes/modules-8
for further information.

After installation the File ID formatter will be available anywhere you can
select a field formatter.

-- SUPPORT --

For support, to report any bugs, or request any features please use the
drupal.org issue queue at https://drupal.org/project/issues/fidformatter
